// using dom
// Retrieve an element from webpage
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name')
/*alternative in retrieving an element - getElement
	document.getElementById('txt-first-name')
	document.getElementsByClass('txt-input')
	document.getElementsByTagName('input')
*/

	 txtFirstName.addEventListener('keyup',fullName);
	 txtLastName.addEventListener('keyup',fullName);
	 function fullName() {
	 	spanFullName.innerHTML = txtFirstName.value + " " +txtLastName.value
	 }


/*

// Assign same event to multiple listeners
txtFirstName.addEventListener('keyup',(event)  =>{
	// Contains the elemet where the event happened
	console.log(event.target);
	// Gets the value of the iput object
	console.log(event.target.value);
	})*/